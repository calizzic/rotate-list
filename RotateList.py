# Rotates singly linked list k locations to the right
class ListNode(object):
     def __init__(self, val=0, next=None):
         self.val = val
         self.next = next

class Solution(object):
    def rotateRight(self, head, k):
        """
        :type head: ListNode
        :type k: int
        :rtype: ListNode
        """
        self.head = head
        if not self.head:
            return self.head
        if not self.head.next:
            return self.head
        current = self.head
        length = 1
        while current.next:
            current = current.next
            length +=1
        self.head = head
        for x in range(k%length):
            self.rotate()
        return self.head
    
    def rotate(self):
        last = self.head
        current = self.head.next
        while current.next:
            last = current
            current = current.next
        last.next = None
        head = self.head
        current.next = head
        self.head = current
        return